const backEnd = document.getElementById("backEnd");
const backEndValue = document.querySelector(".frontEnd .processes__percentageNum").innerHTML;
const frontEnd = document.getElementById("frontEnd");
const frontEndValue = document.querySelector(".backEnd .processes__percentageNum").innerHTML;
const design = document.getElementById("design");
const designValue = document.querySelector(".design .processes__percentageNum").innerHTML;
const copyright = document.getElementById("copyright");
const copyrightValue = document.querySelector(".copyright .processes__percentageNum").innerHTML;

function setProgress(domElement, value) {

   const radius = domElement.r.baseVal.value;
   const circumference = 2 * Math.PI * radius;
   var inputNum = Number(value);
   domElement.style.strokeDasharray = `${circumference} ${circumference}`;
   domElement.style.strokeDashoffset = circumference;

   setProgressValue(domElement, inputNum);


   function setProgressValue(domElement, percent) {
      const offset = circumference - percent / 100 * circumference;
      domElement.style.strokeDashoffset = offset;
   }
}
setProgress(backEnd, frontEndValue);
setProgress(frontEnd, backEndValue);
setProgress(design, designValue);
setProgress(copyright, copyrightValue);

var chart = new Chartist.Line('.ct-chart', {
   series: [
      [0, 2, 1, 5]
   ]
}, {
   low: 0,
   showArea: true,
   showPoint: false,
   fullWidth: true,
   axisX: {
      showLabel: false,
      showGrid: false,
      offset: 0
   },
   axisY: {
      showLabel: false,
      showGrid: false,
      offset: 0
   }

});

chart.on('draw', function (data) {
   if (data.type === 'line' || data.type === 'area') {
      data.element.animate({
         d: {
            begin: 2000 * data.index,
            dur: 2000,
            from: data.path.clone().scale(1, 0).translate(0, data.chartRect.height()).stringify(),
            to: data.path.clone().stringify(),
            easing: Chartist.Svg.Easing.easeOutQuint
         }
      });
   }
});
